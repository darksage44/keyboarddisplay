import os
import tkinter as tk
from tkinter import ttk
import keyboard
import pathlib

#TODOS
# Add one for helicopter mode. Dunno which controls though
# Add space in dir to see if it still works properly
# save settings to appdata to store
#   position of window
#   all options
# Tidy up the images so letters on keys match position
# APM display that shows how many keys are pressed per second or minute
    # gather actions for previous 5 seconds
    # multiply by 60/5

def PopupMSG(msg):
    global optionsPosX
    global optionsPosY
    popup = tk.Toplevel()
    popup.geometry('+' + str(optionsPosX + 20) + '+' + str(optionsPosY + 100))
    popup.wm_title("!")
    label = ttk.Label(popup, text=msg)
    label.pack(side="top", fill="x", pady=10)
    B1 = ttk.Button(popup, text="Okay", command = popup.destroy)
    B1.pack()

def OnExit():
    try:
        optionsWindow.destroy()
        root.destroy()
    except:
        pass

working_dir = str(pathlib.Path().absolute())
#working_dir = 'C:\Data\Code\Python\Keyboardthing' #TODO REMOVE THIS BEFORE SHIPPING

root = tk.Tk()
optionsWindow = tk.Toplevel()
optionsWindow.protocol("WM_DELETE_WINDOW", OnExit)

# Formatting globals
BGColor = 'gray'

windowWidth = 500
windowHeight = 500

# TODO update this position when the main window moves
windowPosX = 100
windowPosY = 250

optionsWidth = 250
optionsHeight = 150

# TODO update this position when the options window moves
optionsPosX = 100
optionsPosY = 500

windowGeometry = str(windowWidth) + 'x' + str(windowHeight) + '+' + str(windowPosX) + '+' + str(windowPosY)
optionsGeometry = str(optionsWidth) + 'x' + str(optionsHeight) + '+' + str(optionsPosX) + '+' + str(optionsPosY)

windowCenter = windowWidth/2

keyPadding = 4

# Offsets when window is disabled so that enabling/disabling they stay in the same spot
disabledWindowWidgetOffsetX = 8
disabledWindowWidgetOffsetY = 31

# Label stuff
keyWidth = 64 # Based on png dimensions
keyHeight = 64 # Based on png dimensions
spaceWidth = 256 # Based on png dimensions
spaceHeight = 64 # Based on png dimensions

keys = {
    'w': ['\\w.png', '\\wd.png'],
    'a': ['\\a.png', '\\ad.png'],
    's': ['\\s.png', '\\sd.png'],
    'd': ['\\d.png', '\\dd.png'],
    'space': ['\\space.png', '\\spaced.png'],
    }

# Theme folders that contain images
themeResDir = '\\Resources\\Themes'
themeDir = working_dir + themeResDir
themeFolders = os.listdir(themeDir)
themes = {}
for theme in themeFolders:
    themeFullDir = themeDir + '\\' + theme

    imageFIles = []
    for f in os.listdir(themeFullDir + '\\'):
        imageFIles.append(f)
    
    missingFiles = []
    for k in keys.values():
        for png in k:
            wasFound = False
            for f in imageFIles:
                addslash = '\\' + f # Add slash because the png in keys is stored with a slash to make appending easier later
                if png == addslash:
                    wasFound = True
                    break
            if not wasFound:
                missingFiles.append(png)
    
    # If any files were missing make a popup that says which are missing
    if len(missingFiles) > 0:
        msgstr = "Theme: " + theme + " is missing files: "
        skipFirst = True
        msgstr += missingFiles[0]
        for mf in missingFiles[1:]:
            msgstr += ", " + mf
        print(msgstr)
        PopupMSG(msgstr)
    else:
        themes[theme] = themeResDir + '\\' + theme

# Set current theme to the first one in the list
#TODO override this with saved setting
currentTheme = list(themes.keys())[0]

def OnPress(event):
    lowerkey = str(event.name).lower()
    if lowerkey in labelInfo.keys():
        labelInfo[lowerkey][0].configure(image=labelInfo[lowerkey][2])
        labelInfo[lowerkey][0].image = labelInfo[lowerkey][2]
        

def OnRelease(event):
    lowerkey = str(event.name).lower()
    if lowerkey in labelInfo.keys():
        labelInfo[lowerkey][0].configure(image=labelInfo[lowerkey][1])
        labelInfo[lowerkey][0].image = labelInfo[lowerkey][1]

def OnAlphaSliderModified(event):
    root.wm_attributes("-alpha", alphaSlider.get()/100)

def OnEnableWindowCheckbox():
    if bv.get():
        for value in labelInfo.values():
            value[0].place(x=value[3], y=value[4])
        root.overrideredirect(False)
        root.wm_attributes("-disabled", False)
    else:
        for value in labelInfo.values():
            value[0].place(x=value[3] + disabledWindowWidgetOffsetX, y=value[4] + disabledWindowWidgetOffsetY)
        root.overrideredirect(True)
        root.wm_attributes("-disabled", True)

def OnEnableBGCheckbox():
    if bgbv.get():
        root.wm_attributes("-transparentcolor", "")
    else:
        root.wm_attributes("-transparentcolor", BGColor)

def ApplyCurrentTheme():
    for k in keys.keys():
        labelInfo[k][1] = tk.PhotoImage(file=working_dir + themes[currentTheme] + keys[k][0])
        labelInfo[k][2] = tk.PhotoImage(file=working_dir + themes[currentTheme] + keys[k][1])
        labelInfo[k][0].configure(image=labelInfo[k][1])
        labelInfo[k][0].image = labelInfo[k][1]

def themeCallback(event):
    global currentTheme
    currentTheme = themeCombobox.get()
    ApplyCurrentTheme()

#root.after(16, update)
#def update():
#    if keyboard.is_pressed('a'):
#        print('A is pressed')
#    root.after(16, update)

# BG label used to make transparent window
bglabel = tk.Label(root, bg=BGColor)

# Slider to control alpha of whole window
alphaLabel = tk.Label(optionsWindow, text="Transparency")
alphaSlider = tk.Scale(optionsWindow, from_=0, to=100, orient=tk.HORIZONTAL, command=OnAlphaSliderModified)
alphaSlider.set(100)

enableWindowLabel = tk.Label(optionsWindow, text="Movable Window")
bv = tk.BooleanVar()
enableWindowCheckbox = tk.Checkbutton(optionsWindow, variable=bv, command=OnEnableWindowCheckbox)

enableBGLabel = tk.Label(optionsWindow, text="Enable Background")
bgbv = tk.BooleanVar()
enableBGCheckbox = tk.Checkbutton(optionsWindow, variable=bgbv, command=OnEnableBGCheckbox)

themeLabel = tk.Label(optionsWindow, text="Theme")
themeCombobox = ttk.Combobox(optionsWindow, state="readonly")
builtArr = []
for item in themes:
    builtArr.append(item)
themeCombobox["values"] = builtArr
themeCombobox.set(currentTheme)
themeCombobox.bind("<<ComboboxSelected>>", themeCallback)

# Setup a dictionary to make referencing the key data later easier
labelInfo = {
    'w': [tk.Label(root, bg=BGColor), tk.PhotoImage(), tk.PhotoImage(), windowCenter-(keyWidth/2),           keyHeight * 0],
    'a': [tk.Label(root, bg=BGColor), tk.PhotoImage(), tk.PhotoImage(), windowCenter-(keyWidth/2)-keyWidth - keyPadding,  keyHeight * 1 + keyPadding],
    's': [tk.Label(root, bg=BGColor), tk.PhotoImage(), tk.PhotoImage(), windowCenter-(keyWidth/2),           keyHeight * 1 + keyPadding],
    'd': [tk.Label(root, bg=BGColor), tk.PhotoImage(), tk.PhotoImage(), windowCenter-(keyWidth/2)+keyWidth + keyPadding,  keyHeight * 1 + keyPadding],
    'space': [tk.Label(root, bg=BGColor), tk.PhotoImage(), tk.PhotoImage(), windowCenter-(spaceWidth/2), (keyHeight + keyPadding) * 2],
}

ApplyCurrentTheme()

optionsInfo = {
    'AlphaLabel':           [alphaLabel,           25,  18],
    'AlphaSlider':          [alphaSlider,          105, 0],
    'EnableWindowLabel':    [enableWindowLabel,    25,  50],
    'EnableWindowCheckbox': [enableWindowCheckbox, 150, 48],
    'EnableBGLabel':        [enableBGLabel,        25,  80],
    'EnableBGCheckbox':     [enableBGCheckbox,     150, 78],
    'ThemeLabel':           [themeLabel,           25,  110],
    'ThemeCombobox':        [themeCombobox,        80,  110]
    }

def onHotkey(realKey):
    print("hotkey " + realKey)

# Register all the keys
for key in labelInfo:
    keyboard.on_press_key(key, OnPress)
    keyboard.on_release_key(key, OnRelease)

#keyboard.on_press_key('esc', OnExit)

root.title("Keyboard Display")
root.geometry(windowGeometry)
root.lift()
root.wm_attributes("-topmost", True)
root.overrideredirect(True)
root.wm_attributes("-disabled", True)
root.wm_attributes("-transparentcolor", BGColor)

optionsWindow.title("Options")
optionsWindow.geometry(optionsGeometry)
optionsWindow.lift()
#optionsWindow.wm_attributes("-topmost", True)

# Pack all the labels
bglabel.place(x=0, y=0, width=windowWidth, height=windowHeight)
bglabel.lower()
for value in labelInfo.values():
    value[0].place(x=value[3] + disabledWindowWidgetOffsetX, y=value[4] + disabledWindowWidgetOffsetY)

# Pack all the options
for value in optionsInfo.values():
    value[0].place(x=value[1], y=value[2])

root.mainloop()
