ECHO OFF

cd ../
rmdir /s /q "installer/Distribution/"

rem This is the loose build stuff
rem robocopy ./Resources/ ./installer/Temp/Resources /S /MIR
rem pyinstaller ./installer/Temp/KeyboardDisplay.spec --distpath ./installer/Distribution --workpath ./installer/Temp -y

robocopy ./Resources/ ./installer/Distribution/Resources /S /MIR
pyinstaller ./installer/Temp/KeyboardDisplay.spec --distpath ./installer/Distribution --workpath ./installer/Temp -y -F

cd installer
