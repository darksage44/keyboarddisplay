ECHO OFF

cd ../
rmdir /s /q "installer/Temp/"

rem This is the loose build stuff
rem robocopy ./Resources/ ./installer/Temp/Resources /S /MIR
rem pyinstaller KeyboardDisplay.pyw --distpath ./installer/Temp --workpath ./installer/Temp/Work --specpath ./installer/Temp -y --add-data Resources;Resources

rem This is the one file exe. Have to copy Resources manually
robocopy ./Resources/ ./installer/Temp/Resources /S /MIR
pyinstaller KeyboardDisplay.pyw --distpath ./installer/Temp --workpath ./installer/Temp/Work --specpath ./installer/Temp -y -F

cd installer
